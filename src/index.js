import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import Main from 'components/Main';
import Grid from 'components/Grid';
import Single from 'components/Single';
import PopUpWindow from 'components/PopUpWindow';

const router = (
  <Router history = { browserHistory }>
    <Route path = "/" component = { Main }>
      <IndexRoute component = { Grid }></IndexRoute>
      <Route path = "/view/:postId" component = { Single }></Route>
      <Route path='*' component={NotFound}></Route>
    </Route>
  </Router>
)

const NotFound = () => <h1>404.. This page is not found!</h1>

render(router, document.getElementById('root'));
