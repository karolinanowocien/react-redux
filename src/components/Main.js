import React from 'react';
import Grid from './Grid';
import { Link } from 'react-router';


const Main = React.createClass({
  render() {
    return (
      <div className='main'>
        <header>
          <div className='main__header'>
              <h1>
              <Link to="/">Film app</Link>
              </h1>
          </div>
        </header>
        <Grid />
      </div>
    );
  }
});

export default Main;
