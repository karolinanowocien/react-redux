//////////////////////////////////

//add film
//////////

//add title
//add category
//add description
//add picture

//cancel
//save
//////////////////////////////////

//edit Film
///////////
//edit title
//edit category
//edit description
//edit picture

//cancel
//save
//delete
//////////////////////////////////

//add comments
export function addComment(postId, author, comment){
  return{
    type: 'ADD_COMMENT',
    postId,
    author,
    comment
  }
}
//remove comments
export function removeComment(postId, index){
  return{
    type: 'REMOVE_COMMENT',
    postId,
    index
  }
}
//increment likes
export function increment(index){
  return{
    type: 'INCREMENT_LIKES',
    index
  }
}
