const comments = {


  "pjx-gQcVi":[

  ],
  "oTZ0zQcWt":[
    {
      "text":"Love the coat! Where's it from? Lol",
      "user": "_lindersss"
    }
  ],
  "mxKQoQcQh":[

  ],
  "fasqlQceO":[
    {
      "text":"Where's lux at? 💤?",
      "user": "henrihelvetica"
    },
    {
      "text":"Love this house during the holidays! And all other times of the year...",
      "user": "danielleplas"
    }
  ],
  "VBgtGQcSf":[
    {
      "text":"@dogsandbrew",
      "user": "likea_bos"
    },
    {
      "text":"Next on my list!",
      "user": "tomwalsham"
    },
    {
      "text":"Is that from collective arts ?",
      "user": "trevorb_91"
    }
  ],
  "FpTyHQcau":[
    {
      "text":"@kaitbos  that vest!!! 😍",
      "user": "courtneyeveline"
    },
    {
      "text":"I just love her! @kaitbos",
      "user": "kalibrix"
    },
    {
      "text":"@courtneyeveline I know! My friend gave it to her and I wanted a matching one but only Lux can pull it off. She's so fancy 😉",
      "user": "kaitbos"
    },
    {
      "text":"Char has that vest!!! Super cute!",
      "user": "jennlensink"
    },
    {
      "text":"You'll have to meet her soon @kalibrix!!",
      "user": "kaitbos"
    },
    {
      "text":"Haha @kaitbos so true, babies these days are sooo stylish. 😎",
      "user": "courtneyeveline"
    },
    {
      "text":"JavaScript 😄😆🙋",
      "user": "lucascaixeta"
    },
    {
      "text":"That hoodie is amazing! Where did you get it?",
      "user": "br11x"
    },
    {
      "text":"@br11x I did a teespring a few months ago - maybe I'll do another one soon",
      "user": "wesbos"
    }
  ]
};

export default comments;
