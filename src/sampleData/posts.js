const posts = [

   {
      "code":"-pjx-gQcVi",
      "caption":"My baby and me. Thanks to @bearandsparrow for this one.",
      "likes":81,
      "id":"1128590547628442978",
      "display_src":"http://www.flickeringmyth.com/wp-content/uploads/2015/09/ma-ma-poster.jpg"
   },
   {
      "code":"-oTZ0zQcWt",
      "caption":"It's too early. Send coffee.",
      "likes":81,
      "id":"1128237044221461933",
      "display_src":"http://i0.wp.com/www.idesigni.co.uk/blog/wp-content/uploads/2014/08/Drive-James-White.jpg"
   },
   {
      "code":"-mxKQoQcQh",
      "caption":"They both have figured it out. #lifewithsnickers",
      "likes":47,
      "id":"1127804966031967265",
      "display_src":"https://s-media-cache-ak0.pinimg.com/236x/9d/72/fb/9d72fb7b36b9a1faa6e2dbd4f70d7658.jpg"
   },
   {
      "code":"-fasqlQceO",
      "caption":"Kaitlin decorated the house for the Christmas. So gezellig! #casabos",
      "likes":46,
      "id":"1125735850454402958",
      "display_src":"https://s-media-cache-ak0.pinimg.com/564x/fd/5e/66/fd5e662dce1a3a8cd192a5952fa64f02.jpg"
   },
   {
      "code":"-VBgtGQcSf",
      "caption":"Trying the new Hamilton Brewery beer. Big fan.",
      "likes":27,
      "id":"1122810327591928991",
      "display_src":"http://payload114.cargocollective.com/1/9/314138/4585380/POSTER.jpg"
   },
   {
      "code":"-FpTyHQcau",
      "caption":"I'm in Austin for a conference and doing some training. Enjoying some local brew with my baby.",
      "likes":82,
      "id":"1118481761857291950",
      "display_src":"https://s-media-cache-ak0.pinimg.com/236x/07/21/13/072113bf4e0abbeab0a98a842def0e9b.jpg"
   }
];


export default posts;
